﻿using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class VillagerThirdPersonController: MonoBehaviour
	{
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(0.01f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
		[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
		[SerializeField] float m_MoveSpeedMultiplier = 1f;
		[SerializeField] float m_AnimSpeedMultiplier = 1f;
        [SerializeField] float m_AirMoveSpeedMultiplier = 3f;
        [SerializeField] float m_GroundCheckDistance = 0.1f;
		[SerializeField] int m_NumberOfAirborneJumps = 0;
        [SerializeField] bool m_CantMoveWhileGrounded;
        [SerializeField] string m_Starting_Animal = "human";

		Rigidbody m_Rigidbody;
		Animator m_Animator;
		bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
		int m_JumpsSinceGrounded = 0;
		Vector3 m_OrigMove;

		GameObject DogMask = null;
		GameObject HawkMask = null;
		GameObject DeerMask = null;
		void Awake()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
			m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;

			Transform x  = transform.Find ("Villager2/metarig/hips/spine/chest/neck/head/DogSpiritMask");
			if (x!=null){DogMask = x.gameObject;}
			x  =  transform.Find ("Villager2/metarig/hips/spine/chest/neck/head/BirdSpiritMask");
			if (x!=null){HawkMask=x.gameObject;}
			x  =  transform.Find ("Villager2/metarig/hips/spine/chest/neck/head/DeerSpiritMask");
			if (x!=null){DeerMask=x.gameObject;}

			ChangeAnimal (m_Starting_Animal);
		}

		public void Move(Vector3 move, bool crouch, bool jump)
		{
			// convert the world relative moveInput vector into a local-relative
			// turn amount and forward amount required to head in the desired
			// direction.
			m_OrigMove = move;
			if (move.magnitude > 1f) move.Normalize();
			move = transform.InverseTransformDirection(move);
			CheckGroundStatus();
			move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			m_TurnAmount = Mathf.Atan2(move.x, move.z);
			m_ForwardAmount = move.z;

			ApplyExtraTurnRotation();

			// control and velocity handling is different when grounded and airborne:
			if (m_IsGrounded)
			{
				HandleGroundedMovement(crouch, jump);
			}
			else
			{
				HandleAirborneMovement(Vector3.zero, jump);
			}

			ScaleCapsuleForCrouching(crouch);
			PreventStandingInLowHeadroom();

			float vy = m_Rigidbody.velocity.y;
			m_Rigidbody.velocity = new Vector3 (m_OrigMove.x * m_MoveSpeedMultiplier, vy, m_OrigMove.z * m_MoveSpeedMultiplier) ;

			// send input and other state parameters to the animator
			UpdateAnimator(move);
		}


		void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, ~0, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}

        public void Move(Vector3 cameraMove, Vector3 worldMove, bool crouch, bool jump, bool cantJumpWhileStanding)
        {

            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            m_OrigMove = cameraMove;
            if (cameraMove.magnitude > 1f) cameraMove.Normalize();
            cameraMove = transform.InverseTransformDirection(cameraMove);
            CheckGroundStatus();
            cameraMove = Vector3.ProjectOnPlane(cameraMove, m_GroundNormal);
            m_TurnAmount = Mathf.Atan2(cameraMove.x, cameraMove.z);
            m_ForwardAmount = cameraMove.z;

            ApplyExtraTurnRotation();

            // control and velocity handling is different when grounded and airborne:
            if (m_IsGrounded)
            {
                var lJump = jump;
                if (cantJumpWhileStanding && !(worldMove.x > 0.2 || worldMove.z > 0.2))
                {
                    lJump = false;
                }

                HandleGroundedMovement(crouch, jump);
            }
            else
            {
                HandleAirborneMovement(worldMove, jump);
            }

            int cantMove = 1;
            if (m_IsGrounded && m_CantMoveWhileGrounded)
            {
                cantMove = 0;
            }

            ScaleCapsuleForCrouching(crouch);
            PreventStandingInLowHeadroom();

            float vy = m_Rigidbody.velocity.y;
            m_Rigidbody.velocity = new Vector3(m_OrigMove.x * m_MoveSpeedMultiplier * cantMove, vy, m_OrigMove.z * m_MoveSpeedMultiplier * cantMove);

            // send input and other state parameters to the animator
            UpdateAnimator(cameraMove);
        }

        void PreventStandingInLowHeadroom()
		{
			// prevent standing up in crouch-only zones
			if (!m_Crouching)
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, ~0, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
				}
			}
		}

		public void updateGameObject(GameObject gameObject, bool b){
			if (gameObject != null) {
				gameObject.SetActive (b);
			}
		}

		public void ChangeAnimal ( string animal){
			animal = animal.ToLower ();
			m_Animator.SetBool ("IsHuman", false);
			m_Animator.SetBool ("IsDog", false);
			m_Animator.SetBool ("IsHawk", false);
			m_Animator.SetBool ("IsGecko", false);
			m_Animator.SetBool ("IsDeer", false);

			updateGameObject (DogMask, false);//.SetActive (false);
			updateGameObject (HawkMask, false);
			updateGameObject (DeerMask, false);
			if (animal == "human") {
				m_Animator.SetBool ("IsHuman", true);
			} else if (animal == "dog") {
				m_Animator.SetBool ("IsDog", true);
				updateGameObject (DogMask, true);
			} else if (animal == "hawk") {
				m_Animator.SetBool ("IsHawk", true);
				updateGameObject (HawkMask, true);
			} else if (animal == "gecko") {
				m_Animator.SetBool ("IsGecko", true);
			} else if (animal == "deer") {
				m_Animator.SetBool ("IsDeer", true);
				updateGameObject (DeerMask, true);
			}
		}

        public bool IsHuman()
        {
            return m_Animator.GetBool("IsHuman");
        }

		void UpdateAnimator(Vector3 move)
		{
			// update the animator parameters
			m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
			m_Animator.SetBool("Crouch", m_Crouching);
			m_Animator.SetBool("OnGround", m_IsGrounded);
			if (!m_IsGrounded)
			{
				m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
			}
			if (m_ForwardAmount != 0) {
				m_Animator.SetBool ("IsWalking", true);
			} else {
				m_Animator.SetBool ("IsWalking", false);
			}

			// calculate which leg is behind, so as to leave that leg trailing in the jump animation
			// (This code is reliant on the specific run cycle offset in our animations,
			// and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
			float runCycle =
				Mathf.Repeat(
					m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
			float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
			if (m_IsGrounded)
			{
				m_Animator.SetFloat("JumpLeg", jumpLeg);
			}

			// the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
			// which affects the movement speed because of the root motion.
			if (m_IsGrounded && move.magnitude > 0)
			{
				m_Animator.speed = m_AnimSpeedMultiplier;
			}
			else
			{
				// don't use that while airborne
				m_Animator.speed = 1;
			}
		}


		void HandleAirborneMovement(Vector3 move, bool doubleJump)
		{
			// apply extra gravity from multiplier:
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);

            m_Rigidbody.velocity = new Vector3(-move.x * m_AirMoveSpeedMultiplier, m_Rigidbody.velocity.y, -move.z * m_AirMoveSpeedMultiplier);

            // TODO: Do we need to check for the m_animator.GetCurrentAnimatorStateInfo part?
            //if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
            // jump!
            if (doubleJump && m_JumpsSinceGrounded < m_NumberOfAirborneJumps)
			{
				m_JumpsSinceGrounded = ++m_JumpsSinceGrounded;
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
                m_Animator.SetTrigger("DoubleJump");
			}

			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}


		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// check whether conditions are right to allow a jump:
			if (jump && !crouch) {
				// jump!
				m_Rigidbody.velocity = new Vector3 (m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
			} 
		}

		void ApplyExtraTurnRotation()
		{
			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}


		public void OnAnimatorMove()
		{
			// we implement this function to override the default root motion.
			// this allows us to modify the positional speed before it's applied.
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

				// we preserve the existing y part of the current velocity.
				v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
			}
		}


		void CheckGroundStatus()
		{
            if (m_Animator == null)
                return;

			RaycastHit hitInfo;
			#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
			#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				m_GroundNormal = hitInfo.normal;
				m_IsGrounded = true;
				m_JumpsSinceGrounded = 0;
				m_Animator.applyRootMotion = true;
			}
			else
			{
				m_IsGrounded = false;
				m_GroundNormal = Vector3.up;
				m_Animator.applyRootMotion = false;
			}
		}

        public void CloneStatus(VillagerThirdPersonController baseObject)
        {
            this.m_MovingTurnSpeed = baseObject.m_MovingTurnSpeed;
            this.m_StationaryTurnSpeed = baseObject.m_StationaryTurnSpeed;
            this.m_JumpPower = baseObject.m_JumpPower;
            this.m_GravityMultiplier = baseObject.m_GravityMultiplier;
            this.m_RunCycleLegOffset = baseObject.m_RunCycleLegOffset;
            this.m_MoveSpeedMultiplier = baseObject.m_MoveSpeedMultiplier;
            this.m_AnimSpeedMultiplier = baseObject.m_AnimSpeedMultiplier;
            this.m_AirMoveSpeedMultiplier = baseObject.m_AirMoveSpeedMultiplier;
            this.m_GroundCheckDistance = baseObject.m_GroundCheckDistance;
            this.m_NumberOfAirborneJumps = baseObject.m_NumberOfAirborneJumps;
            this.m_CantMoveWhileGrounded = baseObject.m_CantMoveWhileGrounded;
        }
	}
}
