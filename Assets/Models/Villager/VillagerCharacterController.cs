﻿using UnityEngine;
using System.Collections;
//test controller
public class VillagerCharacterController : MonoBehaviour {
	Animator Villager;
	void Start () {
		// Set up references.
		Villager = GetComponent <Animator> ();
	}

	void Update () {
		// Store inputs.
		float h1 = Input.GetAxisRaw ("Horizontal"); //walk (Default: left/right) 
		float h2 = Input.GetAxisRaw ("Vertical"); 	//walkBackwards (Default: up/down)
		float h3 = Input.GetAxisRaw ("Fire1"); //Push (Default: left mouse button)
		float h4 = Input.GetAxisRaw ("Fire2"); //Grab (Default: right mouse button)
		float h5 = Input.GetAxisRaw ("Fire3"); //Look (Default: center mousebutton/ click scrollwheel)
		float h6 = Input.GetAxisRaw ("Jump"); //sleep (Default: spacebar)
		float h7 = Input.GetAxisRaw ("Mouse ScrollWheel"); //wake (Default: move scrollwheel)
		bool jump = Input.GetKey(KeyCode.C);

		// Boolean that is true if either of the input axies is non-zero. idle is true if no other inputs .
		bool walking = (h1 != 0f || h2!= 0f);
		bool isDog = h3 != 0f;
		bool isHawk = h4 != 0f;
		bool isGecko = h5 != 0f;
		bool isDeer = h6 != 0f;
		bool isHuman = h7 != 0f;
		//bool botIdle = h1 == 0f && h2 == 0f && h3 == 0f && h4 == 0f&& h5 == 0f&& h6 == 0f && h7 == 0f;

		// Call to animate Grov.
		Animating (walking, isDog, isHawk, isGecko, isDeer, isHuman, jump);
	}

	void Animating (bool walking, bool isDog, bool isHawk, bool isGecko, bool isDeer, bool isHuman,bool jump) {
		// Tell the animator what Grov is doing.
		Villager.SetBool ("IsWalking", walking);
		Villager.SetBool ("IsDog", isDog);
		Villager.SetBool ("IsHawk", isHawk);
		Villager.SetBool ("IsGecko", isGecko);
		Villager.SetBool ("IsDeer", isDeer);
		Villager.SetBool ("IsHuman", isHuman);
		Villager.SetBool ("Grounded", !jump);
	}

}
