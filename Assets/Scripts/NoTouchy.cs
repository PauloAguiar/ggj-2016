﻿using UnityEngine;
using System.Collections;

public class NoTouchy : MonoBehaviour {

    public GameObject RespawnPoint;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Player")
        {
            collision.collider.gameObject.transform.position = RespawnPoint.transform.position;
        }
    }
}
