﻿using UnityEngine;
using System.Collections;

public class Resource : MonoBehaviour 
{
	public bool isBones;
	public bool isBook;
	public bool isCandle;
	public bool isCoins;
	public bool isGems;
	public bool isGlitter;
	private bool isActive = false;
	private Villager villager;

	void OnTriggerEnter(Collider hit)
	{
		if (hit.transform.tag == "Player") 
		{
			villager = hit.transform.GetComponent<Villager>();
			isActive = true;
		}
	}

	void OnTriggerExit(Collider hit)
	{
		if (hit.transform.tag == "Player") 
		{
			villager = null;
			isActive = false;
		}
	}

	void Update()
	{
		if (isActive && !villager.isHolding) 
		{
			if (Input.GetKeyDown (KeyCode.E)) 
			{
				if ((villager.farmingSkill == 3 && isBones || isCoins) || 
					(villager.learningSkill == 3 && isBook || isGlitter) ||
					(villager.miningSkill == 3 && isGems || isCandle))
				{
					villager.currentLoad = villager.carryCap;
				}

				else if ((villager.farmingSkill == 2 && isBones || isCoins) ||
						(villager.learningSkill == 2 && isBook || isGlitter) ||
						(villager.miningSkill == 2 && isGems || isCandle))
				{
					villager.currentLoad = villager.carryCap / 2;
				}

				else if ((villager.farmingSkill == 1 && isBones || isCoins) ||
						(villager.learningSkill == 1 && isBook || isGlitter) ||
						(villager.miningSkill == 1 && isGems || isCandle))
				{
					villager.currentLoad = villager.carryCap / 4;
				}

				if (isBones) 
				{
					villager.isBones = true;
				}

				else if (isBook) 
				{
					villager.isBook = true;
				}

				else if (isCandle) 
				{
					villager.isCandle = true;
				}

				else if (isCoins) 
				{
					villager.isCoins = true;
				}

				else if (isGems) 
				{
					villager.isGems = true;
				}

				else if (isGlitter) 
				{
					villager.isGlitter = true;
				}

				villager.isHolding = true;

				Destroy (gameObject);
			}
		}
	}
}