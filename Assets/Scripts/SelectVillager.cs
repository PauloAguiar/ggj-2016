﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.Characters.ThirdPerson;

public class SelectVillager : MonoBehaviour
{
    [SerializeField]
    private Spawner spawnerOne;

    [SerializeField]
    private Spawner spawnerTwo;

    [SerializeField]
    private Spawner spawnerThree;

    [SerializeField]
    private FreeLookCam cam;


    void Update()
    {
		if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            cam.SetTarget(spawnerOne.GetSpawned());
            spawnerOne.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = true;
            spawnerTwo.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;
            spawnerThree.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;

        }

		if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            cam.SetTarget(spawnerTwo.GetSpawned());
            spawnerOne.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;
            spawnerTwo.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = true;
            spawnerThree.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;
        }

		if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            cam.SetTarget(spawnerThree.GetSpawned());
            spawnerOne.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;
            spawnerTwo.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = false;
            spawnerThree.GetSpawned().GetComponent<VillagerThirdPersonControl>().enabled = true;
        }
    }
}
