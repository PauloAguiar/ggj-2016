﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Cameras;

public class ChangeCharacter : MonoBehaviour
{
    [SerializeField]
    string animal;
    [SerializeField]
    GameObject basePrefab;
    [SerializeField]
    GameObject sceneCamera;
    [SerializeField]
    GameObject baseCamera;

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Triggered");
        if(collider.gameObject.tag == "Player")
        {
			var controller = collider.gameObject.GetComponent<VillagerThirdPersonController> ();
			if (!controller.IsHuman ())
				return;

			Debug.Log ("Changing to " + animal);
			var baseController = basePrefab.GetComponent<VillagerThirdPersonController> ();
			var control = collider.gameObject.GetComponent<VillagerThirdPersonControl> ();
			var baseControl = basePrefab.GetComponent<VillagerThirdPersonControl> ();
			var freeLookCamera = sceneCamera.GetComponent<FreeLookCam> ();
			var baseFreeLookCamera = baseCamera.GetComponent<FreeLookCam> ();
			controller.ChangeAnimal (animal);
			controller.CloneStatus (baseController);
			control.CloneStatus (baseControl);
			freeLookCamera.CloneStatus (baseFreeLookCamera);
			Destroy (gameObject);
        }
    }
}
