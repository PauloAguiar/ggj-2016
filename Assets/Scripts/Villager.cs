﻿using UnityEngine;
using System.Collections;

public class Villager : MonoBehaviour 
{
	public int farmingSkill;
	public int learningSkill;
	public int miningSkill;
	public int carryCap;
	public int currentLoad;
	[HideInInspector] public bool hasOffered;
	[HideInInspector] public bool isHolding;
	[HideInInspector] public bool isBones;
	[HideInInspector] public bool isBook;
	[HideInInspector] public bool isCandle;
	[HideInInspector] public bool isCoins;
	[HideInInspector] public bool isGems;
	[HideInInspector] public bool isGlitter;
}
