﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Altar : MonoBehaviour 
{
	private bool isActive = false;
	private Villager villager;
	private ParticleSystem fire;
	bool candle;
	bool bone;
	bool gem;
	GameObject ender;

	void Start()
	{
		fire = transform.Find ("Flame").GetComponent<ParticleSystem>();
		ender = GameObject.Find ("End Screen");
		ender.SetActive (false);
	}

	void OnTriggerEnter(Collider hit)
	{
		if (hit.transform.tag == "Player") 
		{
			villager = hit.transform.GetComponent<Villager>();
			isActive = true;
		}
	}

	void OnTriggerExit(Collider hit)
	{
		if (hit.transform.tag == "Player") 
		{
			villager = null;
			isActive = false;
		}
	}

	void Update () 
	{
		if (isActive && villager.isHolding) 
		{
			if (Input.GetKeyDown (KeyCode.E)) 
			{
				if (villager.isBones) 
				{
					bone = true;
				} 

				else if (villager.isCandle) 
				{
					candle = true;
				} 
					
				else if (villager.isGems) 
				{
					gem = true;
				}
					
				fire.Play (withChildren: true);
				villager.isHolding = false;
				villager.hasOffered = true;
				transform.GetComponent<AudioSource> ().Play ();
				Test ();
			}
		}
	}

	void Test()
	{
		if (candle && bone && gem) 
		{
			ender.SetActive (true);
		}
	}
}
