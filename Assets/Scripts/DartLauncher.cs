﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DartLauncher : MonoBehaviour {
    [SerializeField]
    GameObject Dart;

    [SerializeField]
    float m_DelayBeforeFirstShot = 0;

    [SerializeField]
    float m_DelayBeforeSubsequentShots = 1;

    [SerializeField]
    float m_DartSpeed = 1;
    [SerializeField]
    int m_NumberOfDarts = 10;

    float timeSinceLastShot = 0;
    bool firstShotFired = false;
    List<GameObject> darts = new List< GameObject >();

    [SerializeField]
    GameObject RespawnPoint;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        timeSinceLastShot += Time.fixedDeltaTime;
        if (!firstShotFired)
        {
            if (timeSinceLastShot >= m_DelayBeforeFirstShot)
            {
                FireShot();
                firstShotFired = true;
                timeSinceLastShot -= m_DelayBeforeFirstShot;
            }
        }
        else
        {
            if (timeSinceLastShot >= m_DelayBeforeSubsequentShots)
            {
                FireShot();
                timeSinceLastShot -= m_DelayBeforeSubsequentShots;
            }
        }

        var dartNumber = 1;
        List<GameObject> dartsToDelete = new List<GameObject>();
        foreach (GameObject d in darts)
        {
            d.GetComponent<Rigidbody>().MovePosition(d.transform.position + d.transform.rotation * Vector3.forward * m_DartSpeed * Time.fixedDeltaTime);
            dartNumber++;
            if( dartNumber > m_NumberOfDarts)
            {
                dartsToDelete.Add(d);
            }
        }
        foreach(GameObject d in dartsToDelete)
        {
            darts.Remove(d);
            Destroy(d);
        }
        
    }

    void FireShot()
    {
        var theDart = (GameObject)Instantiate(Dart, transform.position, transform.rotation);
        theDart.GetComponent<NoTouchy>().RespawnPoint = RespawnPoint;
        theDart.transform.parent = transform;
        darts.Insert(0, theDart);
    }
}
