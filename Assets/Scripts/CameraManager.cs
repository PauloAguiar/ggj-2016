﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class CameraManager : MonoBehaviour {
    [SerializeField]
    GameObject freelookCameraPrefab;
    static GameObject freelookCamera;
    [SerializeField]
    GameObject initialCamera;   

    static GameObject currentCameraGameObject;

	void Start () {
        freelookCamera = freelookCameraPrefab;
        if(initialCamera != null)
        {
            currentCameraGameObject = Instantiate(initialCamera);
            currentCameraGameObject.transform.parent = this.transform;
        }
	}

    static public void SetCurrentCameraToFreelook(GameObject target)
    {
        currentCameraGameObject = Instantiate(freelookCamera);
        currentCameraGameObject.GetComponent<FreeLookCam>().SetTarget(target.transform);
    }
    
    static public void DisableCurrentCamera()
    {
        Destroy(currentCameraGameObject);
    }
}
