﻿using UnityEngine;
using System.Collections;

public class VillagerStatsUI : MonoBehaviour 
{
	private Transform stat01;
	private Transform stat02;
	private Transform stat03;
	private Transform stat04;
	private Transform stat05;
	private Transform stat06;
	private Transform stat07;
	private Transform stat08;
	private Transform stat09;

	void Start()
	{
		stat01 = transform.Find ("Skill 01").GetChild (0);
		stat02 = transform.Find ("Skill 01").GetChild (1);
		stat03 = transform.Find ("Skill 01").GetChild (2);
		stat04 = transform.Find ("Skill 02").GetChild (0);
		stat05 = transform.Find ("Skill 02").GetChild (1);
		stat06 = transform.Find ("Skill 02").GetChild (2);
		stat07 = transform.Find ("Skill 03").GetChild (0);
		stat08 = transform.Find ("Skill 03").GetChild (1);
		stat09 = transform.Find ("Skill 03").GetChild (2);
		stat01.gameObject.SetActive (false);
		stat02.gameObject.SetActive (false);
		stat03.gameObject.SetActive (false);
		stat04.gameObject.SetActive (false);
		stat05.gameObject.SetActive (false);
		stat06.gameObject.SetActive (false);
		stat07.gameObject.SetActive (false);
		stat08.gameObject.SetActive (false);
		stat09.gameObject.SetActive (false);
	}

	public void Change(Villager vila)
	{
		if (vila.farmingSkill == 1) 
		{
			stat01.gameObject.SetActive (true);
			stat02.gameObject.SetActive (false);
			stat03.gameObject.SetActive (false);
		}

		else if (vila.farmingSkill == 2) 
		{
			stat01.gameObject.SetActive (true);
			stat02.gameObject.SetActive (true);
			stat03.gameObject.SetActive (false);
		}

		else if (vila.farmingSkill == 3) 
		{
			stat01.gameObject.SetActive (true);
			stat02.gameObject.SetActive (true);
			stat03.gameObject.SetActive (true);
		}

		if (vila.learningSkill == 1) 
		{
			stat04.gameObject.SetActive (true);
			stat05.gameObject.SetActive (false);
			stat06.gameObject.SetActive (false);
		}

		else if (vila.learningSkill == 2) 
		{
			stat04.gameObject.SetActive (true);
			stat05.gameObject.SetActive (true);
			stat06.gameObject.SetActive (false);
		}

		else if (vila.learningSkill == 3) 
		{
			stat04.gameObject.SetActive (true);
			stat05.gameObject.SetActive (true);
			stat06.gameObject.SetActive (true);
		}

		if (vila.miningSkill == 1) 
		{
			stat07.gameObject.SetActive (true);
			stat08.gameObject.SetActive (false);
			stat09.gameObject.SetActive (false);
		}

		else if (vila.miningSkill == 2) 
		{
			stat07.gameObject.SetActive (true);
			stat08.gameObject.SetActive (true);
			stat09.gameObject.SetActive (false);
		}

		else if (vila.miningSkill == 3) 
		{
			stat07.gameObject.SetActive (true);
			stat08.gameObject.SetActive (true);
			stat09.gameObject.SetActive (true);
		}
	}
}
