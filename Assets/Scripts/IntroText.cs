﻿using UnityEngine;
using System.Collections;

public class IntroText : MonoBehaviour 
{
	public float timer;
	float timerTemp;
	GameObject text1;
	GameObject camMain;
	GameObject camIntro;
	GameObject splash;
	int stage;
	GameObject chooser;

	void Start () 
	{
		camMain = GameObject.Find ("Deer Camera");
		camIntro = GameObject.Find ("Camera Intro");
		text1 = transform.Find ("Text 1").gameObject;
		camMain.SetActive (false);
		text1.SetActive (false);
		splash = GameObject.Find ("Splash Screen");
		chooser = GameObject.FindObjectOfType<SelectVillager> ().gameObject;
		chooser.SetActive (false);
	}

	void Update()
	{
		timerTemp += Time.deltaTime;
		if (timerTemp >= timer) 
		{
			if (stage == 0) 
			{
				timerTemp = 0;
				stage = 1;
				text1.SetActive (true);
				Destroy (splash);
			}

			else if (stage == 1) 
			{
				text1.SetActive (false);
				camMain.SetActive (true);
				Destroy (camIntro);
				chooser.SetActive (true);
				transform.GetComponent<AudioSource> ().Stop ();
				Destroy (gameObject);
			}
		} 
	}
}
