﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.Characters.ThirdPerson;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject spawnableObjectPrefab;

    private GameObject spawned;

    void Awake()
    {
        if (spawnableObjectPrefab == null)
            throw new MissingReferenceException();
    }

    void Start()
    {
        spawned = Instantiate(spawnableObjectPrefab, this.transform.position, this.transform.rotation) as GameObject;

        var villagerControl = spawned.GetComponent<VillagerThirdPersonControl>();
        var nonVillagerControl = spawned.GetComponent<ThirdPersonUserControl>();
        if (villagerControl) villagerControl.enabled = false;
        if (nonVillagerControl) nonVillagerControl.enabled = false;

        var spawnedVillager = spawned.AddComponent<Villager>();
        var villagerStats = GetComponent<Villager>();

        spawnedVillager.farmingSkill = villagerStats.farmingSkill;
        spawnedVillager.learningSkill = villagerStats.learningSkill;
        spawnedVillager.miningSkill = villagerStats.miningSkill;
        spawnedVillager.carryCap = villagerStats.carryCap;
        spawnedVillager.currentLoad = villagerStats.currentLoad;
    }

    public Transform GetSpawned()
    {
        return spawned.transform;
    }
}
