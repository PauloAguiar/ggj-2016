﻿using UnityEngine;
using System.Collections;

public class GrovCharacterController : MonoBehaviour {
	Animator Grov;
	
	void Start () {
		// Set up references.
		Grov = GetComponent <Animator> ();
	}
	
	void FixedUpdate () {
		// Store inputs.
		float h1 = Input.GetAxisRaw ("Horizontal"); //walk (Default: left/right) 
		float h2 = Input.GetAxisRaw ("Vertical"); 	//walkBackwards (Default: up/down)
		float h3 = Input.GetAxisRaw ("Fire1"); //Push (Default: left mouse button)
		float h4 = Input.GetAxisRaw ("Fire2"); //Grab (Default: right mouse button)
		float h5 = Input.GetAxisRaw ("Fire3"); //Look (Default: center mousebutton/ click scrollwheel)
		float h6 = Input.GetAxisRaw ("Jump"); //sleep (Default: spacebar)
		float h7 = Input.GetAxisRaw ("Mouse ScrollWheel"); //wake (Default: move scrollwheel)

		// Boolean that is true if either of the input axies is non-zero. idle is true if no other inputs .
		bool walking = h1 != 0f;
		bool walkingBackwards = h2 != 0f;
		bool push = h3 != 0f;
		bool grab = h4 != 0f;
		bool look = h5 != 0f;
		bool sleep = h6 != 0f;
		bool wake = h7 != 0f;
		bool botIdle = h1 == 0f && h2 == 0f && h3 == 0f && h4 == 0f&& h5 == 0f&& h6 == 0f && h7 == 0f;

		// Call to animate Grov.
		Animating (walking, walkingBackwards, push, grab, look, sleep, wake, botIdle);
	}

	void Animating (bool walking, bool walkingBackwards, bool push, bool grab, bool look, bool sleep, bool wake, bool botIdle) {
		// Tell the animator what Grov is doing.
		Grov.SetBool ("IsMoving", walking);
		Grov.SetBool ("IsMovingBackwards", walkingBackwards);
		Grov.SetBool ("IsPushing", push);
		Grov.SetBool ("IsGrabing", grab);
		Grov.SetBool ("IsLooking", look);
		Grov.SetBool ("IsSleeping", sleep);
		Grov.SetBool ("IsAwake", wake);
		Grov.SetBool ("IsIdle", botIdle);
	}


}
